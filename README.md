# README #

### What is this repository for? ###

Sentimental Analysis also known as Emotional AI or Opinion mining is the use of language processing and analysis to study, extract and identify subjective information.

It is basically the measurement of positive and negative languages.

The project uses news sentimental analysis to illustrate how stock prices and news articles are related.

Work Flow:

* Sentimental Analysis is performed on news articles scrapped from news websites to generate corresponding Sentiment Scores
* Stock prices were collected from Yahoo finance
* Sentiment scores generated and Stock prices collected are uploaded to Azure Cloud
* Relationship between both are visualized through Graphs.

### How do I get set up? ###

* Install python 2
* Download and save news_stock_sentiment_analysis.py python file in to local system
* open command promt from the folder containing news_stock_sentiment_analysis.py file
* Run command -> python news_stock_sentiment_analysis.py
* Install all the required python dependencies ( it will automatically ask when you run the code )

### Contribution guidelines ###

* Please feel free to contribute to the project.

### Who do I talk to? ###

* If you have any queries regarding the code, please feel to contact me at eldho.kuriyan@gmail.com

### License ###

The product is licensed under apache-2.0, because it is a widely used licese and is supports open source allowing anyone to use or modify the code. This is an opensource public project and anyonw wants to develop applications on top of this, under this license they have the full freedom to do so.